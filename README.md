Your immediate supervisor tells you that higher-ups in the investors are very interested in learning from the past. In particular, they want to know how much money they could have made if they had invested optimally.




Your supervisor gives you the following data on the prices of select stocks in 2011 and in 2021:

| company      | prices in 2011 | prices in 2021 |
| ------------ | -------------- | -------------- |
| Dale, Inc.   | $12            |      $39       |
| JCN Corp.    | $10            |      $13       |
| Macroware    | $18            |      $47       |
| Pear Inc     | $15            |      $45       |


Your supervisor asks you to write an algorithm for computing the best way to purchase stocks, given the initial money *total*, the number *count* of companies with stock available, an array *start* containing the prices of each stock in 2011, and an array *end* containing the prices of each stock in 2022. All prices are assumed to be positive integers.
    
    
   
    
  