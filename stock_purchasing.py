def stock_table(t, c, s, e):
    profit = {}
    purchase = {}
    # for range in no of distinct stocks
    for cash in range(t):
        profit[cash, 0] = cash
        purchase[cash, 0] = False
        for stock in range(c):
            profit[cash, stock] = profit[cash, stock - 1]
            purchase[cash, stock] = False
            if s[stock] <= cash:
                left_over = cash - s[stock]
                current = e[stock] + profit[left_over, stock - 1]
                if profit[cash, stock] <= current:
                    profit[cash, stock] = current
                    purchase[cash, stock] = True
    return purchase

def stock_limited(t, c, s, e, l):
    profit = {}
    for cash in range(t):
        profit[cash, 0] = 0
        for stock in range(c):
            profit[cash, stock] = profit[cash, stock-1]
            maximum = min(l[stock], cash / s[stock])
            for quantity in range(maximum):
                left_over = cash - quantity * s[stock]
                current = e[stock] * quantity + profit[left_over, stock-1]
                if profit[cash, stock] <= current:
                    profit[cash, stock] = current
    return profit[t, c]



                    

def stock_result(t,c,s,e,purchase):
    result = {}
    #  for range of distinct stocks
    for stock in range(c):
        result[stock] = 0
    cash = t
    stock = c

    while stock > 0:
        if purchase[cash, stock]:
            cash = cash - s[stock]
            result[stock] = result[stock] + 1
        stock = stock - 1
    return result

            

